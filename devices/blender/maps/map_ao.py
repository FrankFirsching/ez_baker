import bpy
from .map import EZB_Map_Blender, Map_Context


class Map_Context_AO(Map_Context):
    
    def __enter__(self):
        pass_name, context = super().__enter__()

        if not self.map.isolate:
            for obj in bpy.context.visible_objects:
                if obj.name not in self.scene.objects and not obj.name.endswith('preview_cage'):
                    self.scene.collection.objects.link(obj)
            context['visible_objects'] = self.scene.collection.objects
            context['view_layer'] = {'objects': self.scene.collection.objects}
        return pass_name, context


class EZB_Map_AO(bpy.types.PropertyGroup, EZB_Map_Blender):
    id = 'AO'
    pass_name = 'AO'
    label = 'Ambient Occlusion'
    icon = 'SHADING_RENDERED'
    category = 'Lighting'

    suffix: bpy.props.StringProperty(default='_AO')
    samples: bpy.props.IntProperty(name='Samples', default=128)

    active: bpy.props.BoolProperty(default=True)
    bake: bpy.props.BoolProperty(default=False)

    background_color = [0.5, 0.5, 0.5, 1.0]

    context = Map_Context_AO

    isolate: bpy.props.BoolProperty(default=True)

    def _draw_info(self, layout):
        super()._draw_info(layout)
